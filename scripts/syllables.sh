#!/bin/bash

word=$(tr 'a-z' 'A-Z' <<< $1)
DICT=$(dirname $0)/../dict

egrep "^$word," $DICT | cut -d, -f2

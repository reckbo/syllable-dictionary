Creates a word syllable dictionary by downloading and parsing the 
[CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict).

Requires:

* [redo](https://github.com/mildred/redo.git)

Build
-----

    redo

How to Use
----------

    scripts/syllables.sh aword


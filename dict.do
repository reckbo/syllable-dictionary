#!/bin/bash

cmudict=cmudict-stripped

redo-ifchange $cmudict

sed 's/[^0-9]*//g' $cmudict \
    | awk '{ print length }' \
    | paste -d, <(awk '{ print $1 }' < $cmudict) - \
    > $3
